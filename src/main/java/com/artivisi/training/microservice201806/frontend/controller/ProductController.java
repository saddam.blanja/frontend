package com.artivisi.training.microservice201806.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductController {

    @GetMapping("/product/list")
    public ModelMap daftarProduk() {
        return new ModelMap();
    }
}
